using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gunzoom : MonoBehaviour
{
    // Start is called before the first frame update
    int zoom = 20;
    int normal = 60;
    float smooth = 5;

    public Camera fpsCan;
    private bool isZoomed = false;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            isZoomed = !isZoomed;
            Debug.Log("MIRA");
        }
        if (isZoomed == true)
        {
            fpsCan.fieldOfView = Mathf.Lerp(fpsCan.fieldOfView, zoom, Time.deltaTime * smooth);
        }
        else
        {
            fpsCan.fieldOfView = Mathf.Lerp(fpsCan.fieldOfView, normal, Time.deltaTime * smooth);
        }
    }
}
