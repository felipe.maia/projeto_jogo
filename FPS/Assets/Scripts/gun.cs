using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gun : MonoBehaviour
{
    public float dmg = 10f;
    public float range = 100f;
    public Camera fpsCan;
    public ParticleSystem muzzleFlash;
    public GameObject impact;
    public float impactForce = 30f;
    public float fireRate = 15f;
    private float nextFire = 0f;
    public int maxPistolAmmo = 10;
    private int currentPistolAmmo;
    public float reloadTime = 8f;
    private bool isReloading = false;
    public Text ammoDisplay;
    public Animator animator;
    public AudioSource shootingSound;
    public AudioSource reloadSound;

    void start()
    {
        shootingSound = GetComponent<AudioSource>();
        reloadSound = GetComponent<AudioSource>();
        if (currentPistolAmmo == -1)
        {
            currentPistolAmmo = maxPistolAmmo;
        }
    }

    void OnEnable()
    {
        isReloading = false;
        animator.SetBool("Reloading", false);
    }

    void Update()
    {
        if (isReloading)
            return;
        if (currentPistolAmmo <= 0)
        {
            StartCoroutine(reload());
            return;
        }
        if (Input.GetButton("Fire1") && Time.time >= nextFire)
        {
            nextFire = Time.time + 5f / fireRate;
            Shoot();
        }
    }
    IEnumerator reload()
    {
        isReloading = true;
        animator.SetBool("Reloading", true);
        yield return new WaitForSeconds(reloadTime - .25f);
        animator.SetBool("Reloading", false);
        reloadSound.Play();
        yield return new WaitForSeconds(.25f);
        currentPistolAmmo = maxPistolAmmo;
        isReloading = false;
        ammoDisplay.text = currentPistolAmmo.ToString();
    }
    void Shoot()
    {
        muzzleFlash.Play();
        shootingSound.Play();
        currentPistolAmmo--;
        ammoDisplay.text = currentPistolAmmo.ToString();
        RaycastHit hit;
        if (Physics.Raycast(fpsCan.transform.position, fpsCan.transform.forward, out hit, range))
        {
            target body = hit.transform.GetComponent<target>();
            if (body != null)
            {
                body.takeDamage(dmg);
            }
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
            GameObject impactGO = Instantiate(impact, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactGO, 1f);
        }
    }
}
